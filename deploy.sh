yarn build

rm -rf tmp
mkdir tmp
cd tmp

git init
git remote add -f origin git@github.com:LazyBun/budziol-front.git
git config user.name "Krzysztof Piszko"
git config user.email "krzysztof@piszkod.pl"
git pull origin master
rm *
cp ../dist/* ./

git add *
git commit -m "$(date)"
git push --set-upstream origin master

cd ..
rm -rf tmp
